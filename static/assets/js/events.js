$(document).ready(function() {
  new Clipboard('.link-to-burn');

    $('.link-to-burn').click(function() {
        var el = $(this).next();
        el.toggleClass('copied');

        setTimeout(function() {
            el.toggleClass('copied');
        }, 800)
    });

    $('.link-to-burn').mouseenter(function() {
        $(this).next().toggleClass('hidden');
    });

    $('.link-to-burn').mouseleave(function() {
        $(this).next().toggleClass('hidden');
    });
});