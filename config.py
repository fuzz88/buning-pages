# -*- coding: utf-8 -*-

import os

path = os.path.abspath(os.path.dirname(__file__))

config = {
    '/static': {
        'tools.staticdir.on': True,
        'tools.staticdir.dir': 'static'
    },
    'global': {
        'server.socket_port': 4001,
        'engine.autoreload_on': True,
        'tools.encode.on': True,
        'tools.encode.encoding': 'utf-8',
        'tools.decode.on': True,
        'tools.db.on': True,
        'tools.sessions.on': True,
        'tools.staticdir.root': os.path.abspath(os.getcwd())
    }
}
