# -*- coding: utf-8 -*-

import datetime
import string
import random

import cherrypy

from application import model

from sqlalchemy.orm.exc import NoResultFound


class Index:

    @property
    def db(self):
        return cherrypy.request.db

    def __init__(self):
        pass

    def generate_secret(self):
        return ''.join(random.choice(string.digits + string.ascii_lowercase)
                       for _ in range(16))

    def get_messages_in_session(self, session_id):
        try:
            messages = self.db.query(model.BurningMessage).filter(
                model.BurningMessage.session == session_id).all()
        except NoResultFound:
            messages = []
        return messages

    @cherrypy.tools.template
    def index(self):
        if 'secret' not in cherrypy.session:
            cherrypy.session['secret'] = self.generate_secret()
        return {'session_key': cherrypy.session['secret'] or '',
                'messages': self.get_messages_in_session(cherrypy.session['secret']),
                'base_url': cherrypy.request.base}

    @cherrypy.tools.template
    @cherrypy.expose('message')
    @cherrypy.popargs('link')
    def show_message(self, link):
        message = self.db.query(model.BurningMessage).filter(
            model.BurningMessage.link == link).one()
        now = datetime.datetime.now()
        if message.ttl == datetime.timedelta():
            text = message.text
            message.ttl = datetime.timedelta(seconds=1)
            self.db.add(message)
        elif message.created + message.ttl >= now:
            text = message.text
        else:
            text = 'The message had been burned.'
        return {'message': text,
                'base_url': cherrypy.request.base}


class API:

    @property
    def db(self):
        return cherrypy.request.db

    def __init(self):
        pass

    def getTTL(self, interval):
        if interval == '0':
            return datetime.timedelta()
        elif interval == '1':
            return datetime.timedelta(minutes=5)
        elif interval == '2':
            return datetime.timedelta(hours=1)
        elif interval == '3':
            return datetime.timedelta(hours=24)
        else:
            raise RuntimeError('Wrong lifespan.')

    def generateLink(self):
        return ''.join(random.choice(string.digits + string.ascii_lowercase)
                       for _ in range(7))

    @cherrypy.expose('create-message')
    def create_message(self, message='message', lifespan=0):
        self.db.add(model.BurningMessage(text=message, link=self.generateLink(),
                                         created=datetime.datetime.now(),
                                         ttl=self.getTTL(lifespan),
                                         session=cherrypy.session['secret']))
        raise cherrypy.HTTPRedirect(cherrypy.request.base)


def error_page(status, message, **kwargs):
    data = {'base_url': cherrypy.request.base}
    return cherrypy.tools.template._engine.get_template('page/error.html').render(data)
