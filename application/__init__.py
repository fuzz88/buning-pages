# -*- coding: utf-8 -*-

import cherrypy

import config
from application import model
from application import env_mysql_config

from application.toolbox import TemplateTool
from cp_sqlalchemy import SQLAlchemyTool, SQLAlchemyPlugin


def bootstrap():
    cherrypy.tools.template = TemplateTool()

    cherrypy.tools.db = SQLAlchemyTool()

    DB_URI = 'mysql://%s:%s@localhost:3306/doorbell?charset=utf8' % (
        env_mysql_config.username, env_mysql_config.password)

    sqlalchemy_plugin = SQLAlchemyPlugin(
        cherrypy.engine, model.Base, DB_URI,
        echo=True, encoding='utf8', pool_recycle=3600)

    sqlalchemy_plugin.subscribe()
    sqlalchemy_plugin.create()

    cherrypy.config.update(config.config)

    from application import controller

    cherrypy.config.update({'error_page.default': controller.error_page})
    cherrypy.tree.mount(controller.Index(), '/', config.config)
    cherrypy.tree.mount(controller.API(), '/api', config.config)
