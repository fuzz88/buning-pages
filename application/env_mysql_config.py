import os
import sys

try:
    username = os.environ['ENV_MYSQL_USERNAME']
except KeyError:
    print('Please set the environment variable ENV_MYSQL_USERNAME')
    sys.exit(1)

try:
    password = os.environ['ENV_MYSQL_PASSWORD']
except KeyError:
    print('Please set the environment variable ENV_MYSQL_PASSWORD')
    sys.exit(1)
