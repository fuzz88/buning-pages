# -*- coding: utf-8 -*-

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column
from sqlalchemy.types import String, Integer, DateTime, Interval


Base = declarative_base()


class BurningMessage(Base):

    __tablename__ = 'messages'

    id = Column(Integer, primary_key=True)
    text = Column(String(512))
    link = Column(String(128))
    created = Column(DateTime())
    ttl = Column(Interval())
    session = Column(String(16))
